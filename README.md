# OpenML dataset: Cereals

https://www.openml.org/d/1095

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/Cereals.html

Healthy Breakfast

Reference:   Data available at many grocery stores
Authorization:   free use
Description:   Data on several variable of different brands of cereal.
A value of -1 for nutrients indicates a missing observation.
Number of cases:   77
Variable Names:

Name:   Name of cereal
mfr:   Manufacturer of cereal where A = American Home Food Products; G = General Mills; K = Kelloggs; N = Nabisco; P = Post; Q = Quaker Oats; R = Ralston Purina
type:   cold or hot
calories:   calories per serving
protein:   grams of protein
fat:   grams of fat
sodium:   milligrams of sodium
fiber:   grams of dietary fiber
carbo:   grams of complex carbohydrates
sugars:   grams of sugars
potass:   milligrams of potassium
vitamins:   vitamins and minerals - 0, 25, or 100, indicating the typical percentage of FDA recommended
shelf:   display shelf (1, 2, or 3, counting from the floor)
weight:   weight in ounces of one serving
cups:   number of cups in one serving
rating:   a rating of the cereals

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1095) of an [OpenML dataset](https://www.openml.org/d/1095). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1095/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1095/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1095/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

